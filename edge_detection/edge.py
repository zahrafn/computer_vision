#! /usr/bin/env python
import numpy as np
from PIL import Image
import matplotlib.pyplot as plt


######################### Edge ditection ######################### 

def edge_process( img, kernal_x,kernal_y, n, m ):

    pixels = np.zeros( (n,m) )
    new_img = np.zeros( (n,m) )
    
    ##############################
    N = np.zeros((n, m))
    N[ 1:, :] = img[:-1, : ]
    
    S = np.zeros((n, m))
    S[ :-1, :] = img[1:, : ]
    
    W = np.zeros((n, m))
    W[ :, 1:] = img[:, :-1]
    
    E = np.zeros((n, m))
    E[ :, :-1] = img[:, 1:]
    
    
    NE = np.zeros((n, m))
    NE[ 1:, :-1]= img[:-1, 1:]
    
    NW = np.zeros((n, m))
    NW[ 1:, 1:] = img[:-1, :-1]
    
    SE = np.zeros((n, m))
    SE[ :-1, :-1] = img[1:, 1:]
    
    SW = np.zeros((n, m))
    SW[ :-1, 1:] = img[1:, :-1]
    ##############################
    pix_x = np.zeros((n, m))
    pix_Y = np.zeros((n, m))
    
    pix_x = kernal_x[0][0]*NW + kernal_x[0][1]*N + kernal_x[0][2]*NE + kernal_x[1][0]*W + kernal_x[1][1]*img + kernal_x[1][2]*E + kernal_x[2][0]*SW + kernal_x[2][1]*S + kernal_x[2][2]*SE
    pix_y = kernal_y[0][0]*NW + kernal_y[0][1]*N + kernal_y[0][2]*NE + kernal_y[1][0]*W + kernal_y[1][1]*img + kernal_y[1][2]*E + kernal_y[2][0]*SW + kernal_y[2][1]*S + kernal_y[2][2]*SE
    
    scaled_x = np.zeros((n, m))
    scaled_y = np.zeros((n, m))
    
    pix_val = np.zeros((n, m))
    pix_dir = np.zeros((n, m))
    
    pix_val = np.sqrt( np.power(pix_x, 2) +  np.power(pix_y,2)) 
    
    pix_val_n=pix_val*255.0/np.sqrt(2*255.0**2)
    
    pix_dir = np.arctan2(pix_y , pix_x) 

    
    return pix_val,pix_dir


def edge_find(img_name):
    

    input_img = Image.open(img_name +'.jpg')

    im = np.asarray(input_img,dtype=float)
    np.shape(im)
    
    
    im_gray=im[:,:,0]*0
    for i in range(len(im[:,0,0])):
        for j in range(len(im[0,:,0])):
            im_gray[i,j]=np.mean(im[i,j,:])

        
    n=np.shape(im_gray)[0]
    m=np.shape(im_gray)[1] 
        
    
    sobel_x = np.array([[-1, 0, 1], [-2, 0, 2], [-1 ,0 ,1]])
    sobel_y = np.array([[1, 2, 1], [0, 0, 0], [-1 ,-2 ,-1]])

    robert_x = np.array([[-1, 0, 1], [-1, 0, 1], [-1 ,0 ,1]])
    robert_y = np.array([[1, 1, 1], [0, 0, 0], [-1 ,-1 ,-1]])
    
    new_im_sobel,new_dirim_sobel = edge_process( im_gray, sobel_x , sobel_y, n, m )
    new_im_robert,new_dirim_robert = edge_process( im_gray, robert_x , robert_y, n, m )
    
    im_out_sobel = Image.fromarray(np.uint8(new_im_sobel)) 
    im_out_robert = Image.fromarray(np.uint8(new_im_robert))
    
    im_out_sobel_dir = Image.fromarray(np.uint8(new_dirim_sobel)) 
    im_out_robert_dir = Image.fromarray(np.uint8(new_dirim_robert))


    im_out_sobel.save(img_name+"_edge_sobel.jpg")
    im_out_robert.save(img_name+"_edge_robert.jpg")


    im_out_sobel_dir.save(img_name + "_edge_sobel_dir.jpg")
    im_out_robert_dir.save( img_name+"_edge_robert_dir.jpg")
    
   

    

############################################################# 

######################### Threshold ######################### 


def thereshold_each(img_name , type_kernal, threshold):

    new_input_img = Image.open(img_name+"_edge_"+type_kernal+".jpg")
    n_im = np.asarray(new_input_img,dtype=float)
    np.shape(n_im)

    n_n=np.shape(n_im)[0]
    m_n=np.shape(n_im)[1]

    th_im = np.zeros((n_n, m_n))
    th_im[n_im > threshold] = 255.
            
    im_th = Image.fromarray(np.uint8(th_im))
    im_th.save(img_name+"_edge_threshold_"+type_kernal+".jpg")


def threshold(img_name ,threshold_sobel, threshold_robert, threshold_sobel_dir, threshold_robert_dir):

    thereshold_each(img_name , "sobel" , threshold_sobel)
    thereshold_each(img_name , "sobel_dir" , threshold_sobel_dir)

    thereshold_each(img_name , "robert" , threshold_robert)
    thereshold_each(img_name , "robert_dir" , threshold_robert_dir)



###########################################################

######################### Thining #########################

def thin_edges_each_kernal(im):

    
    n_t=np.shape(im)[0]
    m_t=np.shape(im)[1]
    
  

    
    im_tot = np.zeros((n_t, m_t))
    N = np.zeros((n_t, m_t))
    S = np.zeros((n_t, m_t))
    W = np.zeros((n_t, m_t)) 
    E = np.zeros((n_t, m_t))

    
    
    for i in range(1,n_t-1):
        for j in range(1,m_t-1): 
  
            if im[i][j] !=0:
                
                # NORTH
                if im[i-1][j]!= 0 :
                    N[i][j] += 1
                   
                # WEST
                if im[i][j-1]!= 0 :
                    W[i][j] += 1 
                    
                # EAST
                if im[i][j+1]!= 0 :
                    E[i][j] += 1
                                        
                # SOUTH
                if im[i+1][j]!= 0 :
                    S[i][j] != 1

                    
    im_tot = N + W + E + S
    
    
    
    im_copy = np.copy(im)
    # This means is it has more than 2 neighbors  
    im_copy[im_tot < 2. ] = 0.       
            
    return im_copy


def thin_edges(img_name, num_iter):

    new_input_img_s = Image.open(img_name+"_edge_threshold_sobel.jpg")
    n_im_s = np.asarray(new_input_img_s,dtype=float)
    np.shape(n_im_s)

    new_input_img_r = Image.open(img_name+"_edge_threshold_robert.jpg")
    n_im_r = np.asarray(new_input_img_r,dtype=float)
    np.shape(n_im_r)

    for i in range(num_iter):
        im_s = thin_edges_each_kernal(n_im_s)
        im_r =thin_edges_each_kernal(n_im_r)
        n_im_s = im_s
        n_im_r = im_r


    im_thin_s = Image.fromarray(np.uint8(im_s))
    im_thin_s.save(img_name+"_edge_threshold_thin_sobel.jpg")

    im_thin_r = Image.fromarray(np.uint8(im_r))
    im_thin_r.save(img_name+"_edge_threshold_thin_robert.jpg")

###########################################################

######################### Main  #########################

def main_process(img_name, num_iter_thinning):


    ######## Firts : Find edge

    edge_find(img_name)

    ######## Second : Apply threshold

    ## find thresholds based on histogram of each image form edge_find method

    if img_name == "img1":
        threshold_sobel = 45.
        threshold_robert = 40.
        threshold_sobel_dir = 250
        threshold_robert_dir = 250

    if img_name == "img2":
        threshold_sobel = 40.
        threshold_robert = 45.
        threshold_sobel_dir = 250
        threshold_robert_dir = 250

    threshold(img_name ,threshold_sobel, threshold_robert, threshold_sobel_dir, threshold_robert_dir)

    ######## Third : Thinning

    #thin_edges(img_name, num_iter_thinning)


 
# first parameter is the name of image without .jpg 
# second parameter is the number of iteratation for thinning part
main_process('img1',2)






