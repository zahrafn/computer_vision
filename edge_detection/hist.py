#! /usr/bin/env python

import numpy as np
from PIL import Image
import matplotlib.pyplot as plt

new_input_img = Image.open("img1_edge_sobel_dir.jpg")
im = np.asarray(new_input_img,dtype=float)
np.shape(im)

hist, bin_edges = np.histogram(im,bins = range(260))

plt.bar(bin_edges[:-1], hist, width = 1)
plt.xlim(min(bin_edges), max(bin_edges))
plt.show() 
