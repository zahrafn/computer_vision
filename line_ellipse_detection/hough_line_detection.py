#! /usr/bin/env python
import numpy as np
from PIL import Image,ImageFilter, ImageDraw
import time



def hough_line(img):

    thetas = np.deg2rad(np.arange(-90.0, 90.0, 0.5))
    width, height = img.shape
    diag_len = np.ceil(np.sqrt(width * width + height * height))   # max_dist
    rhos = np.linspace(-diag_len, diag_len, diag_len * 2.0)

    cos_t = np.cos(thetas)
    sin_t = np.sin(thetas)
    num_thetas = len(thetas)

    accumulator = np.zeros((2 * diag_len, num_thetas), dtype=np.uint64)
    y_idxs, x_idxs = np.nonzero(img)

    ############## compute  accumulator, thetas and rhos

    for i in range(len(x_idxs)):
        x = x_idxs[i]
        y = y_idxs[i]

        for t_idx in range(num_thetas):
            rho = x * cos_t[t_idx] + y * sin_t[t_idx] + diag_len
            accumulator[rho, t_idx] += 1
            #print "accumulator ",np.nonzero(accumulator)

    return accumulator, thetas, rhos


def find_hough_peaks(accumulator, thetas, rhos, max_vote):

    ############### find the lines from parameters that we found from accumulator
    lines_idx = np.where( accumulator> max_vote)

    lines_rho = rhos[lines_idx[0]]
    lines_theta = thetas[lines_idx[1]]

    im = Image.open('line1.jpg')

    draw = ImageDraw.Draw(im) 

    ################ make and draw lines that we found
    for rho,theta in zip(lines_rho ,lines_theta) :
        a = np.cos(theta)
        b = np.sin(theta)
        x0 = a*rho
        y0 = b*rho
        x1 = int(x0 + 1000*(-b))
        y1 = int(y0 + 1000*(a))
        x2 = int(x0 - 1000*(-b))
        y2 = int(y0 - 1000*(a))
        draw.line((x1,y1, x2,y2), fill=128)
    im_line = Image.fromarray(np.uint8(im))
    im_line.save("line1_hough.jpg")
    im.show()





def main():
    ############################################### pre processing

    input_img = Image.open('line1.jpg')
    width, height = input_img.size

    ############### make the image gray scale
    input_img = input_img.convert('L')

    ############### find edges
    im = input_img.filter(ImageFilter.FIND_EDGES)
    im = im.crop((1, 1, width - 1, height - 1))

    im_edg = Image.fromarray(np.uint8(im))
    im_edg.save("line_edge.jpg")

    im = np.array(im)
    start_time = time.time()

    ################ hough transformation to find accumulator

    accumulator, thetas, rhos = hough_line(im)
    accumulator_img = accumulator * 255.

    im_accumulator= Image.fromarray(np.uint8(accumulator_img))
    im_accumulator.save("line1_accumulator.jpg")

    max_vote = 240

    ################# from accumulator that we computed we find peakes and find lines

    find_hough_peaks(accumulator, thetas, rhos, max_vote)

    end_time = time.time()

    print "Finished processing in %0.2f seconds" % (end_time - start_time)
    print "DONE"

main()
