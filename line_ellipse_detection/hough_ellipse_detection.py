#! /usr/bin/env python
import numpy as np
from PIL import Image,ImageFilter, ImageDraw
import time



def hough_elipse(edge_pix, min_major, min_minor, minor_count_threshold, num_elp_points, threshold_dist, converge_threshold):


    im = Image.open('ellipse1_resize.jpg')
    width, height = im.size
    io = Image.new("RGB", (width, height), "white")
    draw = ImageDraw.Draw(io)

    ###### parameters needed throw code

    scale = 1. / float(num_elp_points)
    t = 2. * np.pi * np.arange(num_elp_points) / float(num_elp_points - 1)
    sin_t = np.sin(t)
    cos_t = np.cos(t)

    ###################################

    for xy_1 in edge_pix:

        ############## finding ellipse parameters

        distance = np.sqrt(((edge_pix - xy_1) ** 2).sum(1))
        ix_dist = distance >= (min_major)

        centers = 0.5 * (edge_pix + xy_1)
        alpha = np.arctan2(edge_pix[:, 1] - xy_1[1], edge_pix[:, 0] - xy_1[0])

        centers = centers[ix_dist]
        alpha = alpha[ix_dist]
        distance = distance[ix_dist]
        a = 0.5 * distance

        sin_alpha = np.sin(alpha)
        cos_alpha = np.cos(alpha)


        for i, xy_2 in enumerate(edge_pix[ix_dist]):


            accumulator = {}

            ############## Find parameters for potential ellipse

            d = np.sqrt(((edge_pix - centers[i]) ** 2).sum(1))
            ix_d = d >= (min_minor/2.)
            d = d[ix_d]
            f = np.sqrt(((edge_pix - xy_2) ** 2).sum(1))
            f = f[ix_d]
            cost = (a[i] ** 2. + d ** 2. - f ** 2.) / (0.00001 + 2. * a[i] * d)

            b_sqr = (a[i]**2. * d**2. * (1.-cost**2.))/(0.00001 + a[i]**2. - d**2. * cost**2.)
            b_sqr[b_sqr < 0.0] = 0.0
            b = np.asarray(np.sqrt(b_sqr), dtype=np.int32)

            ############### finding parameters based on voting

            for eb in b:
                if accumulator.has_key(eb):
                    accumulator[eb]+=1
                elif eb > 0:
                    accumulator[eb] = 1

            accumulator_rev = dict([(v,k) for k,v in accumulator.iteritems()])
            max_freq = max(accumulator_rev.keys())
            bmax = accumulator_rev[max_freq]



            ################### based on the maximum votes we can fnd the ellipse

            if max_freq >= minor_count_threshold and alpha[i] >=0.0 and bmax >= (min_minor/2.):
                x_el_all = np.asarray(centers[i, 0] + a[i] * cos_t * cos_alpha[i] - bmax * sin_t * sin_alpha[i], dtype=np.int32)
                y_el_all = np.asarray(centers[i, 1] + a[i] * cos_t * sin_alpha[i] + bmax * sin_t * cos_alpha[i], dtype=np.int32)


                points_ratio = 0.0

                ##################### find pixels of ellipse we found form the num_elp_points guess
                for j in range(num_elp_points):
                    #x_el, y_el = elData[i]
                    x_el = x_el_all[j]
                    y_el = y_el_all[j]
                    xy_el = np.array((x_el, y_el))

                    #flag = False
                    dist_el_point = np.sqrt(((edge_pix - xy_el) ** 2).sum(1))
                    ix_dist_el_point = dist_el_point <= threshold_dist
                    ellipse_point = edge_pix[ix_dist_el_point]

                    if ellipse_point.shape[0] > 0:
                        points_ratio += scale

                ############################ when we get enough covarage we start drawing the ellipse
                if points_ratio >= converge_threshold:

                    print "###### coveraged ####### \n center ", centers[i], "angle %.2f" % alpha[i], "axes (%.2f,%.2f)" % (a[i], bmax)
                    for k in range(num_elp_points-1):
                        draw.line((x_el_all[k], y_el_all[k], x_el_all[k+1], y_el_all[k+1]), fill = 128)


    im_el = Image.fromarray(np.uint8(io))
    im_el.save("ellipse1_hough.jpg")



def main():
    ############################################### pre processing

    mode = "medium"

    if mode == "medium":
        w, h = 400, 300

    if mode == "small":
        w, h = 200, 150

    input_img = Image.open('ellipse1.jpg')
    input_img = input_img.resize((w, h), Image.ANTIALIAS)
    width, height = input_img.size

    im_small = Image.fromarray(np.uint8(input_img))
    im_small.save("ellipse1_resize.jpg")

    ############### make the image gray scale
    input_img = input_img.convert('L')

    ############### find edges
    im = input_img.filter(ImageFilter.FIND_EDGES)
    im = im.crop((1, 1, width-1, height - 1))

    im_edg = Image.fromarray(np.uint8(im))
    im_edg.save("ellipse1_edge.jpg")

    ############### convert to binary
    im = im.convert('1')

    img = im.load()
    edge_pix =[]

    ############### find edge pixels
    width, height = im.size
    for x in range(width):
        for y in range(height):
            if img[x,y]==255:
                edge_pix.append((x,y))

    edge_pix = np.array(edge_pix)
    print len(edge_pix)

    ################################################################
    ############### set the parameters of ellipses



    if mode == "medium":

        min_major = 67
        min_minor = 28
        minor_count_threshold= 20
        num_elp_points = 40
        threshold_dist = 0.2
        converge_threshold = 0.7

    if mode == "small":

        min_major = 34
        min_minor = 14
        minor_count_threshold= 20
        num_elp_points = 40
        threshold_dist = 0.2
        converge_threshold = 0.7



    ###############   applying hough transformation

    start_time = time.time()

    hough_elipse(edge_pix, min_major, min_minor, minor_count_threshold, num_elp_points, threshold_dist, converge_threshold)

    end_time = time.time()

    print "Finished processing in %0.2f seconds" % (end_time - start_time)
    print "DONE"

main()














