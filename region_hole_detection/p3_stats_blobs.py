#! /usr/bin/env pjthon

from PIL import Image, ImageDraw
import numpy as np

import random


class DisjointSet:
    def __init__(self):
        self.P = []
        self.label = 0

    def makeLabel(self):
        r = self.label
        self.label += 1
        self.P.append(r)
        return r

    def setRoot(self, i, root):
        while self.P[i] < i:
            j = self.P[i]
            self.P[i] = root
            i = j
        self.P[i] = root

    def findRoot(self, i):
        while self.P[i] < i:
            i = self.P[i]
        return i


    def find(self, i):
        root = self.findRoot(i)
        self.setRoot(i, root)
        return root


    def union(self, i, j):
        if i != j:
            root = self.findRoot(i)
            rootj = self.findRoot(j)
            if root > rootj: root = rootj
            self.setRoot(j, root)
            self.setRoot(i, root)

    def flatten(self):
        for i in range(1, len(self.P)):
            self.P[i] = self.P[self.P[i]]

    def flattenL(self):
        k = 1
        for i in range(1, len(self.P)):
            if self.P[i] < i:
                self.P[i] = self.P[self.P[i]]
            else:
                self.P[i] = k
                k += 1






def raster_scan_label(img):


    im = img.load()
    n, m = img.size

    d_set = DisjointSet()

    labels = {}

    for j in range(m):
        for i in range(n):


            if im[i, j] == 0:
                #labels[i, j] = 0
                pass

            elif j > 0 and im[i, j-1] == 255:
                labels[i, j] = labels[(i, j-1)]


            elif i+1 < n and j > 0 and im[i+1, j-1] == 255:

                c = labels[(i+1, j-1)]
                labels[i, j] = c

                if i > 0 and im[i-1, j-1] == 255:
                    a = labels[(i-1, j-1)]
                    d_set.union(c, a)


                elif i > 0 and im[i-1, j] == 255:
                    d = labels[(i-1, j)]
                    d_set.union(c, d)

            elif i > 0 and j > 0 and im[i-1, j-1] == 255:
                labels[i, j] = labels[(i-1, j-1)]


            elif i > 0 and im[i-1, j] == 255:
                labels[i, j] = labels[(i-1, j)]


            else:
                labels[i, j] = d_set.makeLabel()




    d_set.flatten()

    colors = {}
    num = len(labels)
    blob_lables = {}
    pix_reg = {}

    out_img = Image.new("RGB", (n, m))
    outim = out_img.load()

    for (i, j) in labels:


        region = d_set.find(labels[(i, j)])

        if not region == 0:

            labels[(i, j)] = region


            if region not in colors:
                colors[region] = (random.randint(0,255), random.randint(0,255),random.randint(0,255))

            if region in pix_reg:
                pix_reg [region].append([i,j])
            else:
                pix_reg[region] = [[i,j]]
                #print pix_reg

        else:
                colors[region] = (255, 255, 255)

        outim[i, j] = colors[region]


    #out_img.show()


    return pix_reg, out_img


def stat_blobs(pix_reg,img):

    n, m = img.size
    print 'number of holes: ', len(pix_reg)
    out_img = Image.new("RGB", (n, m))
    outim = out_img.load()

    k = list(pix_reg.keys())
    total_area = 0.0

    for ix, key in enumerate(k):

        area_hole = len(pix_reg[key])
        total_area += area_hole

        # dist = []
        maxdis = 0.0

        all_x = [pix_reg[key][i][0] for i in range(len(pix_reg[key]))]
        all_y = [pix_reg[key][j][1] for j in range(len(pix_reg[key]))]

        for i in range(len(all_x)):
            for j in range(len(all_x)):
                dist = np.sqrt((all_x[j] - all_x[i]) ** 2 + (all_y[j] - all_y[i]) ** 2)
                if dist > maxdis:
                    maxdis = dist
                    x_1 = all_x[i]
                    x_2 = all_x[j]
                    y_1 = all_y[i]
                    y_2 = all_y[j]

        p =  2.*np.pi *(maxdis/2.0)
        # x_cent = int(x_1 + x_2 / 2)
        # y_cent = int(y_1 + y_2 / 2)
        x_centroid = np.sum(all_x)/ len(all_x)
        y_centroid = np.sum(all_y)/ len(all_y)

        min_x, min_y = np.min(all_x), np.min(all_y)
        max_x, max_y = np.max(all_x), np.max(all_x)

        # print 'hole: ', ix+1, 'center: x=', int(x_1 + x_2/2), ' y:', int(y_1 + y_2/2)
        print 'hole: ', ix + 1
        print '\t with area: ', area_hole
        print '\t centeroid: x=', x_centroid, ' y=', y_centroid
        print '\t perimeter: ', p
        print '\t elongation', np.power(p,2)/area_hole
        print '\t MBR: min x=', min_x, ' min y=',min_y, 'max x=', max_x, 'max_y=', max_y

    print "Total area of holes is: ", total_area


def main():


    im_num = ['3', '4']

    for e in im_num:
        # print "here"

        img = Image.open('reg'+e+'_th.jpg')

        img = img.point(lambda p: p > 190 and 255)
        img = img.convert('1')

        pix_reg, out_img = raster_scan_label(img)
        print '\n'
        print '####### reg',e,'statistics ####### '
        stat_blobs(pix_reg, img)

        out_img.save('reg' + e + '_hole.jpg')



main()


