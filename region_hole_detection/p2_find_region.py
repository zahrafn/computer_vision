#! /usr/bin/env pjthon

from PIL import Image, ImageDraw

import random


class DisjointSet:
    def __init__(self):
        self.P = []
        self.label = 0

    def makeLabel(self):
        r = self.label
        self.label += 1
        self.P.append(r)
        return r

    def setRoot(self, i, root):
        while self.P[i] < i:
            j = self.P[i]
            self.P[i] = root
            i = j
        self.P[i] = root

    def findRoot(self, i):
        while self.P[i] < i:
            i = self.P[i]
        return i


    def find(self, i):
        root = self.findRoot(i)
        self.setRoot(i, root)
        return root


    def union(self, i, j):
        if i != j:
            root = self.findRoot(i)
            rootj = self.findRoot(j)
            if root > rootj: root = rootj
            self.setRoot(j, root)
            self.setRoot(i, root)

    def flatten(self):
        for i in range(1, len(self.P)):
            self.P[i] = self.P[self.P[i]]

    def flattenL(self):
        k = 1
        for i in range(1, len(self.P)):
            if self.P[i] < i:
                self.P[i] = self.P[self.P[i]]
            else:
                self.P[i] = k
                k += 1






def raster_scan_label(img):


    im = img.load()
    n, m = img.size

    d_set = DisjointSet()

    labels = {}

    for j in range(m):
        for i in range(n):


            if im[i, j] == 255:
                pass

            elif j > 0 and im[i, j-1] == 0:
                labels[i, j] = labels[(i, j-1)]


            elif i+1 < n and j > 0 and im[i+1, j-1] == 0:

                c = labels[(i+1, j-1)]
                labels[i, j] = c

                if i > 0 and im[i-1, j-1] == 0:
                    a = labels[(i-1, j-1)]
                    d_set.union(c, a)


                elif i > 0 and im[i-1, j] == 0:
                    d = labels[(i-1, j)]
                    d_set.union(c, d)

            elif i > 0 and j > 0 and im[i-1, j-1] == 0:
                labels[i, j] = labels[(i-1, j-1)]


            elif i > 0 and im[i-1, j] == 0:
                labels[i, j] = labels[(i-1, j)]


            else:
                labels[i, j] = d_set.makeLabel()




    d_set.flatten()

    colors = {}

    out_img = Image.new("RGB", (n, m))
    outim = out_img.load()

    for (i, j) in labels:


        region = d_set.find(labels[(i, j)])

        labels[(i, j)] = region

        # Associate a random color with this region
        if region not in colors:
            colors[region] = (random.randint(0,255), random.randint(0,255),random.randint(0,255))

        outim[i, j] = colors[region]

    #out_img.show()
    return  out_img



def main():


    im_num = ['3', '4']

    for e in im_num:
        # print "here"

        img = Image.open('reg'+e+'_th.jpg')

        img = img.point(lambda p: p > 190 and 255)
        img = img.convert('1')

        out_img = raster_scan_label(img)

        out_img.save('reg' + e + '_region.jpg')




main()

