#! /usr/bin/env python

import numpy as np
from PIL import Image
import matplotlib.pyplot as plt

def thresholding(img, im_num):
    #print "here"

    n1, m1 = np.shape(img)

    im = np.zeros((n1, m1))
    im = img[4:-4, 4:-4 ]

    n, m = np.shape(im)

    max_a = int(np.amax(im))


    hist, bin_edges = np.histogram(im,bins = range(max_a+1))

    #################### plotting original histogram

    plt.bar(bin_edges[:-1], hist, width = 1)
    plt.xlim(min(bin_edges), max(bin_edges))
    plt.show()



    ################### Smoothing the histogram

    neigh_size = 3
    ext_hist = np.zeros(len(hist)+ 2*neigh_size)
    ext_hist[neigh_size:-neigh_size] = hist
    new_hist = np.zeros(len(hist))

    for i in range(neigh_size,len(ext_hist)-2*neigh_size):
        sum_neigh = 0.0
        for j in range(neigh_size):
            neigh = ext_hist[i+(j+1)] + ext_hist[i-(j+1)]
            sum_neigh += neigh

        new_hist[i] = (ext_hist[i] + sum_neigh)/(2.*neigh_size + 1 )



    ################## plotting smoothed histogram

    plt.bar(bin_edges[:-1], new_hist, width = 1)
    plt.xlim(min(bin_edges), max(bin_edges))
    plt.show()

    ############################# thresholding

    sig = np.zeros(len(bin_edges))
    bins = bin_edges[:-1]
    total_pix = np.sum(new_hist)
    prob = new_hist/total_pix

    for k in range(len(bins)):
        if np.sum(prob[0:k]) == 0:
            continue
        w0 = np.sum(prob[0:k])
        w1 = 1-w0

        mu0 = np.sum(bins[:k] * prob[:k] ) / w0
        mu1 = np.sum(bins[k:] * prob[k:]) / w1
        sig[k] = w0 * w1 * (np.power((mu1 - mu0), 2))

    threshold = np.argmax(sig)


    th_im = np.zeros((n, m))
    th_im[im > threshold] = 255.
    im_th = Image.fromarray(np.uint8(th_im))
    im_th.save('reg'+im_num+'_th.jpg')



################# applying the threshold


def main():
    im_num = ['3', '4']

    for e in im_num:
        #print "here"

        input_img = Image.open('reg'+e+'.jpg')
        img = np.asarray(input_img, dtype=float)
        thresholding(img,e)


main()




